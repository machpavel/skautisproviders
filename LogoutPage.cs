﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace SkautIsProviders
{
    public class LogoutPage : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                UserManagement.UserManagement userManagement = new UserManagement.UserManagement();
                UserIdentity identity = (UserIdentity)HttpContext.Current.User.Identity;

                userManagement.LoginUpdateLogout(new UserManagement.LoginUpdateLogoutInput() { ID = new Guid(identity.UserToken) });
            }

            FormsAuthentication.SignOut();

            string returnUrl = this.Request.QueryString["ReturnUrl"];

            if (returnUrl != null)
            {
                this.Response.Redirect(returnUrl);
            }
            else
            {
                this.Response.Redirect("~/");
            }
        }
    }
}
