﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SkautIsProviders
{
    public enum UnitType
    {
        Troop,
        Centre
    }

    public class Configuration : ConfigurationSection
    {
        public static Configuration GetConfiguration()
        {
            return (Configuration)System.Configuration.ConfigurationManager.GetSection("skautIsProvidersGroup/skautIsProviders");
        }

        [ConfigurationProperty("appId", IsRequired = true)]
        public Guid AppId
        {
            get
            {
                return (Guid)this["appId"];
            }
            set
            {
                this["appId"] = value;
            }
        }

        [ConfigurationProperty("unit", IsRequired = true)]
        public uint Unit
        {
            get
            {
                return (uint)this["unit"];
            }
            set
            {
                this["unit"] = value;
            }
        }

        [ConfigurationProperty("unitType", IsRequired = true)]
        public UnitType UnitType
        {
            get
            {
                return (UnitType)this["unitType"];
            }
            set
            {
                this["unitType"] = value;
            }
        }
    }
}