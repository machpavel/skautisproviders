﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkautIsProviders
{
    public class LoginPage : System.Web.UI.Page
    {
        static Configuration configuration = Configuration.GetConfiguration();
        protected readonly string SKAUTIS_LOGIN_PAGE = "https://is.skaut.cz/Login/?appid=" + configuration.AppId;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            string returnUrl = this.Request.QueryString["ReturnUrl"];
            if (returnUrl != null)
            {
                this.Response.Redirect(SKAUTIS_LOGIN_PAGE + "&ReturnUrl=" + returnUrl);
            }
            else
            {
                this.Response.Redirect(SKAUTIS_LOGIN_PAGE);
            }
        }
    }
}
