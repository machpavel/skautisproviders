﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace SkautIsProviders
{
    public static class UserAuthentication
    {
        public static void AuthenticateRequest(HttpContext context)
        {
            var userIdentity = new UserIdentity(context);

            context.User = new UserPrincipal(userIdentity);
        }

        public static void ProlongAuthentication(HttpContext context)
        {
            AuthenticateRequest(context);

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                UserManagement.UserManagement userManagement = new UserManagement.UserManagement();
                UserIdentity identity = (UserIdentity)HttpContext.Current.User.Identity;

                userManagement.LoginUpdateRefresh(new UserManagement.LoginUpdateRefreshInput() { ID = new Guid(identity.UserToken) });
            }
        }
    }
}
