﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using SkautIsProviders.UserManagement;
using System.Configuration;

namespace SkautIsProviders
{
    class UserIdentity : IIdentity
    {
        const string AUTHORIZED_COOKIE_STRING = "authorized";

        static readonly uint SKAUTIS_UNIT = Configuration.GetConfiguration().Unit;

        HttpRequest request;
        HttpResponse response;

        UserDetailOutput user;

        UserManagement.UserManagement userManagement;

        public UserIdentity(HttpContext context)
        {
            this.request = context.Request;
            this.response = context.Response;

            this.userManagement = new UserManagement.UserManagement();
        }

        public UserIdentity(HttpContext context, string token)
        {
            this.request = context.Request;
            this.response = context.Response;

            this.userToken = token;

            this.userManagement = new UserManagement.UserManagement();
        }

        public string AuthenticationType
        {
            get { return "SkautIs"; }
        }

        public bool IsAuthenticated
        {
            get
            {
                if (user != null)
                {
                    if (!this.IsUserCookieAuthenticated)
                    {
                        var roles = userManagement.UserRoleAll(new UserRoleAllInput() { ID_Login = new Guid(this.UserToken), ID_User = this.User.ID });

                        foreach (var role in roles)
                        {
                            if (role.ID_Unit == Convert.ToInt32(UserIdentity.SKAUTIS_UNIT))
                            {
                                FormsAuthentication.SetAuthCookie(this.UserToken + ";" + AUTHORIZED_COOKIE_STRING, false);
                                return true;
                            }
                        }
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public string Name
        {
            get
            {
                if (!string.IsNullOrEmpty(this.UserToken) && this.User != null)
                {
                    return this.User.UserName;
                }
                else
                {
                    return null;
                }
            }
        }

        private string userToken;
        internal string UserToken
        {
            get
            {
                if (this.userToken == null)
                {
                    HttpCookie cookie = this.request.Cookies[FormsAuthentication.FormsCookieName];

                    if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                    {
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                        this.userToken = ticket == null ? null : ticket.Name.Split(';')[0];

                        return this.userToken;
                    }
                }
                return this.userToken;
            }
        }

        private bool IsUserCookieAuthenticated
        {
            get
            {
                HttpCookie cookie = this.request.Cookies[FormsAuthentication.FormsCookieName];

                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

                if (ticket != null)
                {
                    string[] cookieValues = ticket.Name.Split(';');
                    return cookieValues.Length > 1 && cookieValues[1] == AUTHORIZED_COOKIE_STRING;
                }
                else
                {
                    return false;
                }
            }
        }

        internal UserDetailOutput User
        {
            get
            {
                if (this.user == null)
                {
                    try
                    {
                        this.user = this.userManagement.UserDetail(new UserDetailInput()
                        {
                            ID_Login = new Guid(this.UserToken)
                        });
                    }
                    catch
                    {
                        this.user = null;
                    }
                }
                return this.user;
            }
        }
    }
}
