﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace SkautIsProviders
{
    class UserPrincipal : IPrincipal
    {
        internal UserPrincipal(IIdentity identity)
        {
            this.identity = identity;
        }

        private IIdentity identity;
        public IIdentity Identity
        {
            get { return this.identity; }
        }

        public bool IsInRole(string role)
        {
            RoleProvider provider = new RoleProvider();
            return provider.IsUserInRole(this.identity.Name, role);
        }
    }
}
