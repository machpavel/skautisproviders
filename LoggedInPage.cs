﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SkautIsProviders
{
    public class LoggedInPage : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Token = this.Request.Form["skautIS_Token"];
            this.Role = this.Request.Form["skautIS_IDRole"];
            this.Unit = this.Request.Form["skautIS_IDUnit"];

            if (!string.IsNullOrEmpty(this.Token))
            {
                var userIdentity = new UserIdentity(HttpContext.Current, this.Token);

                HttpContext.Current.User = new UserPrincipal(userIdentity);

                HttpCookie authenticationCookie = System.Web.Security.FormsAuthentication.GetAuthCookie(this.Token, false);
                Response.Cookies.Add(authenticationCookie);

                string returnUrl = this.Request.QueryString["ReturnUrl"];

                if (!string.IsNullOrEmpty(returnUrl))
                {
                    Response.Redirect(returnUrl);
                }
                else
                {
                    Response.Redirect("~/");
                }
            }
        }

        protected string Token { get; private set; }
        protected string Role { get; private set; }
        protected string Unit { get; private set; }
    }
}
