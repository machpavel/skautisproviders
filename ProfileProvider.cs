﻿using SkautIsProviders.OrganizationUnit;
using SkautIsProviders.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SkautIsProviders
{
    public static class ProfileProvider
    {
        static UserManagement.UserManagement userManagement = new UserManagement.UserManagement();
        static OrganizationUnit.OrganizationUnit organizationUnit = new OrganizationUnit.OrganizationUnit();
        static OrganizationUnit.Person person = null;

        static ProfileProvider()
        {
            LoadPersonDetails();
        }

        public static bool IsLoaded
        {
            get { return person != null; }
        }

        public static string FirstName
        {
            get { return person.FirstName; }
        }

        public static string LastName
        {
            get { return person.LastName; }
        }

        public static string NickName
        {
            get { return person.NickName; }
        }

        public static string DisplayName
        {
            get { return person.DisplayName; }
        }

        public static string Email
        {
            get { return person.Email; }
        }

        public static DateTime? Birthday
        {
            get { return person.Birthday; }
        }

        public static Address Address
        {
            get
            {
                return new Address()
                {
                    District = person.AddressDistrict,
                    City = person.City,
                    PostalCode = person.Postcode,
                    Street = person.Street,
                    State = person.State
                };
            }
        }

        public static Address PostalAddress
        {
            get
            {
                return new Address()
                {
                    District = person.PostalDistrict,
                    City = person.PostalCity,
                    PostalCode = person.PostalPostcode,
                    Street = person.PostalStreet,
                    State = person.PostalState
                };
            }
        }

        public static Photo Photo
        {
            get
            {
                if (person.PhotoContent != null)
                {
                    return new Photo
                    {
                        Width = person.PhotoX.Value,
                        Height = person.PhotoY.Value,
                        Size = person.PhotoSize.Value,
                        Extension = person.PhotoExtension,
                        Content = person.PhotoContent
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        public static string School
        {
            get { return person.School; }
        }

        public static string Sex
        {
            get { return person.Sex; }
        }

        public static int? YearFrom
        {
            get { return person.YearFrom; }
        }

        public static string DegreeBehind
        {
            get { return person.DegreeBehind; }
        }

        public static string DegreeInFrontOf
        {
            get { return person.DegreeInFrontOf; }
        }


        private static void LoadPersonDetails()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                UserIdentity identity = (UserIdentity)HttpContext.Current.User.Identity;

                UserDetailOutput user = userManagement.UserDetail(new UserDetailInput()
                {
                    ID_Login = new Guid(identity.UserToken)
                });

                ProfileProvider.person = organizationUnit.PersonDetail(new PersonDetailInput()
                    {
                        ID = user.ID_Person.Value,
                        ID_Login = new Guid(identity.UserToken)
                    });
            }
        }
    }

    public struct Address
    {
        public string State { get; set; }
        public string District { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }

        public override string ToString()
        {
            return this.Street + ", " + this.PostalCode + " " + this.City + ", " + this.District + ", " + this.State;
        }
    }

    public class Photo
    {
        public int Width;
        public int Height;
        public int Size;
        public string Extension;
        public byte[] Content;

        // create Bitmap class here
    }
}
