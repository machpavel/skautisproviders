﻿using SkautIsProviders.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SkautIsProviders
{
    class RoleProvider : System.Web.Security.RoleProvider
    {
        UserManagement.UserManagement userManagement = new UserManagement.UserManagement();
        UserIdentity userIdentity = HttpContext.Current.User.Identity as UserIdentity;

        static readonly Configuration configuration = Configuration.GetConfiguration();

        public RoleProvider()
        {
            if (this.userIdentity == null)
            {
                EnsureUserIdentity();
            }
        }

        private void EnsureUserIdentity()
        {
            SkautIsProviders.UserAuthentication.AuthenticateRequest(HttpContext.Current);
            this.userIdentity = (UserIdentity)HttpContext.Current.User.Identity;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            if (this.userIdentity != null)
            {
                RoleAllOutput[] roles = userManagement.RoleAll(new RoleAllInput() { ID_Login = new Guid(this.userIdentity.UserToken) });
                List<string> retval = new List<string>();
                foreach (RoleAllOutput role in roles)
                {
                    retval.Add(role.DisplayName);
                }
                return retval.ToArray();
            }
            else
            {
                return new string[0];
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            this.EnsureUserIdentity();

            if (this.userIdentity != null && this.userIdentity.User != null)
            {
                var roles = userManagement.UserRoleAll(new UserRoleAllInput() { ID_Login = new Guid(this.userIdentity.UserToken), ID_User = this.userIdentity.User.ID });

                switch (configuration.UnitType)
                {
                    case UnitType.Troop:

                        List<string> retval = new List<string>();

                        foreach (UserRoleAllOutput role in roles)
                        {
                            if (role.ID_Unit == configuration.Unit)
                            {
                                retval.Add(role.Role);
                            }
                        }
                        return retval.ToArray();
                    case UnitType.Centre:
                        throw new NotImplementedException();
                    default:
                        throw new NotImplementedException();
                }
            }
            else
            {
                return new string[0];
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            string[] roles = this.GetRolesForUser(username);

            foreach (string role in roles)
            {
                if (role == roleName)
                {
                    return true;
                }
            }
            return false;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}
